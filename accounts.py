# The config.py file should look exactly like this one but it should have the actual values in the variables.
# We are only doing it this way so that the people who view our code can see what variables they need to set for
# everything to be working correctly
import config

subscription_key = config.subscription_key
endpoint = config.endpoint

mongodb_endpoint = config.mongodb_endpoint
