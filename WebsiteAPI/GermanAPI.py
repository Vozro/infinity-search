from flask import Flask, Blueprint, render_template, request, make_response, redirect, url_for

germanAPI = Blueprint('germanAPI', __name__)

@germanAPI.route('/de', methods=['GET', 'POST'])
def render_spanish_home():
    return render_template('languages/german/home.html')


@germanAPI.route('/about/de')
def render_about():
    return render_template('languages/german/about.html')


@germanAPI.route('/privacy/de')
def render_privacy():
    return render_template('languages/german/privacy.html')


@germanAPI.route('/contact/de')
def render_contact():
    return render_template('languages/german/contact.html')
