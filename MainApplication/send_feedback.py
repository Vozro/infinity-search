import pymongo
import datetime as dt

from accounts import mongodb_endpoint

def send_feedback(message, email=None):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityFeedback']
    col = db['feedback']

    if email is not None and email != '':
        success = col.insert_one({'msg' : str(message), 'time' : dt.datetime.now(), 'email' : email})
    else:
        success = col.insert_one({'msg': str(message), 'time': dt.datetime.now()})

    if success is None:
        return False

    return True


def send_interesting_find(url):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityInteresting']
    col = db['interesting']
    # from urllib.parse import urlparse

    try:
        # parsed = urlparse(url)
        success = col.insert_one({'msg': str(url), 'time': dt.datetime.now()})

        if success is None:
            return False

    except Exception:
        return False

    return True

