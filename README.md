
![banner](banner.png)

# Infinity Search

## Note 
This repository holds the front-end code and how we access our back-end and other web services that we interact with. 

Below are links to our other repositories: 
- [Infinity Search Solo](https://gitlab.com/infinitysearch/infinity-search-solo): Self-hosted version of Infinity Search that doesn't require any subscriptions 
- [Infinity Analytics](https://gitlab.com/infinitysearch/infinity-analytics): Our analytics system  
- [News Engine](https://gitlab.com/infinitysearch/infinity-news): How we index our news 
- [Infinity Bookmarks](https://gitlab.com/infinitysearch/infinity-bookmarks): Bookmark manager 
- [Inquisite](https://gitlab.com/infinitysearch/inquisite): Text analyzer 
- Search Engines (Coming soon): How we create and index results that come from us 
- Infinity Proxy (Coming soon): A web proxy 

# How It Works
We get our results from several sources and then display them to our users without tracking any identifiable information about 
them.  

# Usage

### Note
to run this on your own as is, you'll need to subscribe to the microsoft cognitive services search API and put your account info 
into a file called accounts.py.

### Requirements
Runtime: Python3.7 

### For Running Infinity Search Locally
```shell script
# pip install the required dependencies
pip3.7 install -r requirements.txt
```

Run wsgi.py and then go to [http://127.0.0.1:5000/](http://127.0.0.1:5000/)

### For Running Infinity Search On Your Own Server
It is the same way as deploying any flask application. For more information about deploying Flask apps, 
you can go [here](https://flask.palletsprojects.com/en/1.1.x/deploying).


## Forking Our Project
This project is open source under the [Apache 2.0 License](/LICENSE) and anyone is welcome to fork our project and use it for their own 
purposes.
